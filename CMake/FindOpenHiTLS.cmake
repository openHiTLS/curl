#***************************************************************************
#                                  _   _ ____  _
#  Project                     ___| | | |  _ \| |
#                             / __| | | | |_) | |
#                            | (__| |_| |  _ <| |___
#                             \___|\___/|_| \_\_____|
#
# Copyright (C) Daniel Stenberg, <daniel@haxx.se>, et al.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at https://curl.se/docs/copyright.html.
#
# You may opt to use, copy, modify, merge, publish, distribute and/or sell
# copies of the Software, and permit persons to whom the Software is
# furnished to do so, under the terms of the COPYING file.
#
# This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
# KIND, either express or implied.
#
# SPDX-License-Identifier: curl
#
###########################################################################
find_library(OPENHITLS_BSL_LIBRARY hitls_bsl)
find_library(OPENHITLS_TLS_LIBRARY hitls_tls)
find_library(OPENHITLS_X509_LIBRARY hitls_x509)
find_library(OPENHITLS_CRYPTO_LIBRARY hitls_crypto)

set(OPENHITLS_LIBRARIES "${OPENHITLS_BSL_LIBRARY}" "${OPENHITLS_TLS_LIBRARY}" "${OPENHITLS_X509_LIBRARY}" "${OPENHITLS_CRYPTO_LIBRARY}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenHiTLS DEFAULT_MSG
    OPENHITLS_BSL_LIBRARY OPENHITLS_TLS_LIBRARY OPENHITLS_X509_LIBRARY OPENHITLS_CRYPTO_LIBRARY)

mark_as_advanced(OPENHITLS_BSL_LIBRARY OPENHITLS_TLS_LIBRARY OPENHITLS_X509_LIBRARY OPENHITLS_CRYPTO_LIBRARY)
