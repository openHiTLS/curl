#***************************************************************************
#                                  _   _ ____  _
#  Project                     ___| | | |  _ \| |
#                             / __| | | | |_) | |
#                            | (__| |_| |  _ <| |___
#                             \___|\___/|_| \_\_____|
#
# Copyright (C) Daniel Stenberg, <daniel@haxx.se>, et al.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution. The terms
# are also available at https://curl.se/docs/copyright.html.
#
# You may opt to use, copy, modify, merge, publish, distribute and/or sell
# copies of the Software, and permit persons to whom the Software is
# furnished to do so, under the terms of the COPYING file.
#
# This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
# KIND, either express or implied.
#
# SPDX-License-Identifier: curl
#
#***************************************************************************

dnl ----------------------------------------------------
dnl check for openHiTLS
dnl ----------------------------------------------------
echo '------------------$OPT_OPENHTLS--------------'
AC_DEFUN([CURL_WITH_OPENHITLS], [

if test "x$OPT_OPENHTLS" != xno; then
  _cppflags=$CPPFLAGS
  _ldflags=$LDFLAGS
  ssl_msg=

  if test X"$OPT_OPENHTLS" != Xno; then

    if test "$OPT_OPENHTLS" = "yes"; then
      OPT_OPENHTLS=""
    fi

    if test -z "$OPT_OPENHTLS" ; then
      dnl check for lib first without setting any new path

      AC_CHECK_LIB(hitls_tls, HITLS_New,
      dnl libhilts_tls found, set the variable
       [
         AC_DEFINE(USE_OPENHITLS, 1, [if openHiTLS is enabled])
         AC_SUBST(USE_OPENHITLS, [1])
         OPENHITLS_ENABLED=1
         USE_OPENHITLS="yes"
         ssl_msg="openHiTLS"
         test openhitls != "$DEFAULT_SSL_BACKEND" || VALID_DEFAULT_SSL_BACKEND=yes
        ], [], -lhitls_bsl -lhitls_x509 -lhitls_crypto -lboundscheck)
    fi

    addld=""
    addlib=""
    addcflags=""
    openhitlslib=""
    boundschecklib=""

    if test "x$USE_OPENHITLS" != "xyes"; then
      dnl add the path and test again
      addld="-L$OPT_OPENHTLS/build -L$OPT_OPENHTLS/platform/Secure_C/lib"
      addcflags="-I$OPT_OPENHTLS/include -I$OPT_OPENHTLS/include/bsl -I$OPT_OPENHTLS/include/tls -I$OPT_OPENHTLS/include/crypto -I$OPT_OPENHTLS/x509/include -I$OPT_OPENHTLS/x509/x509_cert/include -I$OPT_OPENHTLS/bsl/asn1/include -I$OPT_OPENHTLS/bsl/sal/include -I$OPT_OPENHTLS/x509/x509_common/include"
      openhitlslib=$OPT_OPENHTLS/build
      boundschecklib=$OPT_OPENHTLS/platform/Secure_C/lib

      LDFLAGS="$LDFLAGS $addld"
      CPPFLAGS="$CPPFLAGS $addcflags"

      AC_CHECK_LIB(hitls_tls, HITLS_New,
       [
       AC_DEFINE(USE_OPENHITLS, 1, [if openHiTLS is enabled])
       AC_SUBST(USE_OPENHITLS, [1])
       OPENHITLS_ENABLED=1
       USE_OPENHITLS="yes"
       ssl_msg="openHiTLS"
       test openhitls != "$DEFAULT_SSL_BACKEND" || VALID_DEFAULT_SSL_BACKEND=yes
       ],
       [
         CPPFLAGS=$_cppflags
         LDFLAGS=$_ldflags
       ], -lhitls_bsl -lhitls_x509 -lhitls_crypto -lboundscheck)
    fi

    if test "x$USE_OPENHITLS" = "xyes"; then
      AC_MSG_NOTICE([detected openHiTLS])
      check_for_ca_bundle=1

      LIBS="-lhitls_tls -lhitls_bsl -lhitls_x509 -lhitls_crypto -lboundscheck $LIBS"

      if test -n "$openhitlslib"; then
        dnl when shared libs were found in a path that the run-time
        dnl linker doesn't search through, we need to add it to
        dnl CURL_LIBRARY_PATH to prevent further configure tests to fail
        dnl due to this
        if test "x$cross_compiling" != "xyes"; then
          CURL_LIBRARY_PATH="$CURL_LIBRARY_PATH:$openhitlslib:$boundschecklib"
          export CURL_LIBRARY_PATH
          AC_MSG_NOTICE([Added $openhitlslib and $boundschecklib to CURL_LIBRARY_PATH])
        fi
      fi
    fi

  fi dnl openHiTLS not disabled

  test -z "$ssl_msg" || ssl_backends="${ssl_backends:+$ssl_backends, }$ssl_msg"
fi

])
